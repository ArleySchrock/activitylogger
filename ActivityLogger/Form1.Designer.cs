﻿namespace ActivityLogger
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnStart = new VIBlend.WinForms.Controls.vButton();
            this.btnFinish = new VIBlend.WinForms.Controls.vButton();
            this.vLabel1 = new VIBlend.WinForms.Controls.vLabel();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.timeLabel = new VIBlend.WinForms.Controls.vLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtDescription);
            this.panel1.Controls.Add(this.vLabel1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(452, 129);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnFinish);
            this.panel2.Controls.Add(this.btnStart);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(306, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(146, 129);
            this.panel2.TabIndex = 0;
            // 
            // btnStart
            // 
            this.btnStart.AllowAnimations = true;
            this.btnStart.BackColor = System.Drawing.Color.Transparent;
            this.btnStart.Location = new System.Drawing.Point(16, 25);
            this.btnStart.Name = "btnStart";
            this.btnStart.RoundedCornersMask = ((byte)(15));
            this.btnStart.RoundedCornersRadius = 1;
            this.btnStart.Size = new System.Drawing.Size(118, 30);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start Activity";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.METROBLUE;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnFinish
            // 
            this.btnFinish.AllowAnimations = true;
            this.btnFinish.BackColor = System.Drawing.Color.Transparent;
            this.btnFinish.Enabled = false;
            this.btnFinish.Location = new System.Drawing.Point(16, 61);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.RoundedCornersMask = ((byte)(15));
            this.btnFinish.RoundedCornersRadius = 1;
            this.btnFinish.Size = new System.Drawing.Size(118, 30);
            this.btnFinish.TabIndex = 1;
            this.btnFinish.Text = "Finish Activity";
            this.btnFinish.UseVisualStyleBackColor = false;
            this.btnFinish.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.METROORANGE;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // vLabel1
            // 
            this.vLabel1.BackColor = System.Drawing.Color.Transparent;
            this.vLabel1.DisplayStyle = VIBlend.WinForms.Controls.LabelItemStyle.TextOnly;
            this.vLabel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.vLabel1.Ellipsis = false;
            this.vLabel1.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.vLabel1.Location = new System.Drawing.Point(0, 0);
            this.vLabel1.Multiline = true;
            this.vLabel1.Name = "vLabel1";
            this.vLabel1.Size = new System.Drawing.Size(306, 25);
            this.vLabel1.TabIndex = 1;
            this.vLabel1.Text = "Activity Description";
            this.vLabel1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.vLabel1.UseMnemonics = true;
            this.vLabel1.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.VISTABLUE;
            // 
            // txtDescription
            // 
            this.txtDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDescription.Location = new System.Drawing.Point(0, 25);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(306, 104);
            this.txtDescription.TabIndex = 2;
            // 
            // timeLabel
            // 
            this.timeLabel.BackColor = System.Drawing.Color.Transparent;
            this.timeLabel.DisplayStyle = VIBlend.WinForms.Controls.LabelItemStyle.TextOnly;
            this.timeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.timeLabel.Ellipsis = false;
            this.timeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.timeLabel.ImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this.timeLabel.Location = new System.Drawing.Point(0, 129);
            this.timeLabel.Multiline = true;
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(452, 59);
            this.timeLabel.TabIndex = 1;
            this.timeLabel.Text = "Elapsed Time:";
            this.timeLabel.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.timeLabel.UseMnemonics = true;
            this.timeLabel.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.VISTABLUE;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 188);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Activity Log Utility";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtDescription;
        private VIBlend.WinForms.Controls.vLabel vLabel1;
        private System.Windows.Forms.Panel panel2;
        private VIBlend.WinForms.Controls.vButton btnFinish;
        private VIBlend.WinForms.Controls.vButton btnStart;
        private VIBlend.WinForms.Controls.vLabel timeLabel;
        private System.Windows.Forms.Timer timer1;
    }
}

