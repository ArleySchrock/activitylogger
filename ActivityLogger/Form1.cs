﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActivityLogger
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            var dir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            StorageDir = Path.Combine(dir, "Activity Logger");
            LogFile = Path.Combine(StorageDir, "My Activity.log");
        }

        private void btnFinish_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = true;
            btnFinish.Enabled = false;
            txtDescription.Enabled = true;

            finishedTime = DateTime.Now;

            StopTimer();

            LogCompletedActivity();

            ActivityInProgress = false;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Description) ||
                string.IsNullOrWhiteSpace(Description))
            {
                MessageBox.Show("Please enter a valid activity description in the text box before clicking start");
                return;
            }

            txtDescription.Enabled = false;
            btnStart.Enabled = false;
            btnFinish.Enabled = true;

            startTime = DateTime.Now;

            StartTimer();

            LogStartedActivity();

            ActivityInProgress = true;
        }

        private void CheckAndInitDirectory()
        {
            if (!Directory.Exists(StorageDir))
            {
                try
                {
                    Directory.CreateDirectory(StorageDir);
                }
                catch { }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ActivityInProgress)
            {
                MessageBox.Show("Can't stop the application when an activity is in progress!");
                e.Cancel = true;
            }
        }

        private void LogCompletedActivity()
        {
            CheckAndInitDirectory();

            string entry = string.Format("Activity, '{0}', Completed At: '{1}', Elapsed Time: {2}",
                Description, finishedTime, finishedTime - startTime);

            File.AppendAllLines(LogFile, new[] { entry });
        }

        private void LogStartedActivity()
        {
            CheckAndInitDirectory();

            string entry = string.Format("Activity, '{0}', Started At: '{1}', Username: {2}",
                Description, startTime, Environment.UserName);

            File.AppendAllLines(LogFile, new[] { entry });
        }

        private void StartTimer()
        {
            timer1.Start();
        }

        private void StopTimer()
        {
            timer1.Stop();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (ActivityInProgress)
            {
                var ts = DateTime.Now - startTime;

                timeLabel.Text = "Elapsed Time: " + ts.ToString("h'h 'm'm 's's'");
            }
        }

        public bool ActivityInProgress
        {
            get;
            set;
        }

        public string Description
        {
            get
            {
                return txtDescription.Text;
            }
            set
            {
                txtDescription.Text = value;
            }
        }

        public string LogFile
        {
            get;
            set;
        }

        public string StorageDir
        {
            get;
            set;
        }

        private DateTime finishedTime = DateTime.MinValue;
        private DateTime startTime = DateTime.MinValue;
    }
}